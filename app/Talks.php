<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Talks extends Model
{
    //
    use SoftDeletes;

    protected $table = "Talks";
    //Primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    protected $fillable = [
        'title', 'description', 'time', 'date'
    ];

    public function attendees()
    {
        return $this->hasMany('App\Attendees');
    }
}
