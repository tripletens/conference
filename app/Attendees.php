<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Attendees extends Model
{
    use SoftDeletes;

    protected $table = "Attendees";
    //Primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    protected $fillable = [
        'fullname', 'email', 'phone', 'address','state'
    ];

    public function talks()
    {
        return $this->belongsTo('App\Talks');
    }
}
