<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class FrontendController extends Controller
{
    //
    public function landing(){
        return view('frontend.home');
    }

    public function view_talks(){

        // $client = new Client();

        // $request = $client->get('http://127.0.0.1:8000/api/talks/view-all');

        // $response = $request->getBody();

        // dd($response);

        // return "true";
    }
    public function add_talk_page()
    {
        return view('frontend.add_talk_page');
    }
    public function process_add_talk(Request $request){

        $client = new Client();

        $api_request = $client->get('http://127.0.0.1:8000/api/talks/view-all');

        $response = $api_request->getBody();

        return $response;
    }
}
