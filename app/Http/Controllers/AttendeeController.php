<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Attendees;
use App\JoinTalk;
class AttendeeController extends Controller
{
    //
    public function add_attendee(Request $request)
    {

        $request_array = [
            'fullname' => $request->fullname,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'state' => $request->state,
        ];


        $validator = Validator::make($request_array, [
            'fullname' => ['required', 'string'],
            'email' => ['required','string'],
            'phone' => ['required'],
            'address' => ['required'],
            'state' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'error',
                'data' => ['info' => $validator->errors()]
            ], 200)->withHeaders([
                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }

        $add = Attendees::create([
            'fullname' => $request->fullname,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'state' => $request->state,
        ]);

        if ($add != true) {
            return response()->json([
                'message' => "error",
                'data' => ['info' => 'Could not create an attendee']
            ], 500)->withHeaders([
                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }

        return response()->json([
            'message' => "success",
            'data' => ['info' => 'Attendee Successfully created']
        ],200)->withHeaders([
            'Cross-Origin-Resource-Policy' =>
            'cross-origin',
            'Access-Control-Allow-Origin' => '*'
        ]);
    }
    public function join_talk(Request $request){
        $request_array = [
            'talks_id' => $request->talks_id,
            'attendees_id' => $request->attendees_id,
        ];


        $validator = Validator::make($request_array, [
            'talks_id' => ['required'],
            'attendees_id' => ['required']
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'error',
                'data' => ['info' => $validator->errors()]
            ], 200)->withHeaders([
                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }

        // $attendee = JoinTalk::where('attendees_id',$request->attendees_id)
        //         ->where('talks_id',$request->talks_id)->get();


        // if(count($attendee) > 0){
        //     return response()->json([
        //         'message' => "error",
        //         'data' => ['info' => 'Attendee has already joined the talk']
        //     ], 200)->withHeaders([
        //         'Cross-Origin-Resource-Policy' =>
        //         'cross-origin',
        //         'Access-Control-Allow-Origin' => '*'
        //     ]);
        // }
        $join = JoinTalk::create([
            'talks_id' => $request->talks_id,
            'attendees_id' => $request->attendees_id,
        ]);

        if ($join != true) {
            return response()->json([
                'message' => "error",
                'data' => ['info' => 'Could not join the talk']
            ], 200)->withHeaders([
                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }

        return response()->json([
            'message' => "success",
            'data' => ['info' => 'Successfully joined the Talk ']
        ],200)->withHeaders([
            'Cross-Origin-Resource-Policy' =>
            'cross-origin',
            'Access-Control-Allow-Origin' => '*'
        ]);
    }
    public function view_attendees(){
        $Attendees = Attendees::all();
        if($Attendees != null){
            return response()->json([
                'message' => 'success',
                'data' => ['info' => $Attendees]
            ], 200)->withHeaders([
                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }
    }
}
