<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Talks;
use App\JoinTalk;
use Illuminate\Support\Facades\DB;
class TalksController extends Controller
{
    //
    public function add_talk(Request $request){
        $request_array = [
            'title' => $request->title,
            'description' => $request->description,
            'time' => $request->time,
            'date' => $request->date,
        ];


        $validator = Validator::make($request_array, [
            'title' => ['required', 'string', 'unique:talks'],
            'description' => ['required'],
            'time' => ['required'],
            'date' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'error',
                'data' =>['info' => $validator->errors()]
            ], 500)->withHeaders([
                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }
        $add = Talks::create([
            'title' => $request->title,
            'description' => $request->description,
            'time' => $request->time,
            'date' => $request->date,
        ]);

        if ($add != true) {
            return response()->json([
                'message' => "error",
                'data' => ['info' => 'Could not create the talk']
            ], 500)->withHeaders([
                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }

        return response()->json([
            'message' => "success",
            'data' => ['info' => 'Talk Successfully created']
        ], 200)->withHeaders([
            'Cross-Origin-Resource-Policy' =>
            'cross-origin',
            'Access-Control-Allow-Origin' => '*'
        ]);

    }
    public function remove_talk(Request $request)
    {
        $request_array = [
            'talk_id' => $request->talk_id,
        ];
        $validator = Validator::make($request_array, [
            'talk_id' => ['required', 'integer'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'error',
                'data' => $validator->errors()
            ],500)->withHeaders([

                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }

        $talk = Talks::find($request->talk_id);

        if ($talk == null) {
            return response()->json([
                'message' => 'error',
                'data' => ['info' => "Talk doesnt exist"],
            ], 404)->withHeaders([

                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }

        $attendees = JoinTalk::where('talks_id', $request->talk_id);

        // // delete all the attendees that joined the talk
        $delete_attendees = $attendees->delete();

        if ($delete_attendees == false) {
            return response()->json([
                'message' => 'error',
                'data' => ['info' => "All Attendees that joined this talk could not be removed"]
            ], 500)->withHeaders([

                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }

        $remove = $talk->delete();
        if ($remove) {
            return response()->json([
                'message' => 'success',
                'data' => ['info' => "Talk removed Successfully"]
            ], 200)->withHeaders([

                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }
    }
    public function view_talks(){
        $talks = Talks::all();
        if($talks != null){
            return response()->json([
                'message' => 'success',
                'data' => ['info' => $talks]
            ], 200)->withHeaders([
                'Cross-Origin-Resource-Policy' =>
                'cross-origin',
                'Access-Control-Allow-Origin' => '*'
            ]);
        }
    }
    public function view_joined_attendees(Request $request){
        $talk_id = $request->talk_id;

        $joined_attendees = DB::table('join_talks')
            ->join('attendees', 'attendees.id', '=', 'join_talks.attendees_id')
            ->join('talks', 'talks.id', '=', 'join_talks.talks_id')
            ->select('join_talks.*', 'attendees.*', 'join_talks.*')
            ->get();

        return response()->withHeaders([
            'X-header' => '*',
            'Access-Control-Allow-Origin' => '*'
        ])->json([
            'message' => 'success',
            'data' => ['info' => $joined_attendees]
        ], 200);
    }
}
