<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JoinTalk extends Model
{
    //
    protected $table = "join_talks";
    //Primary Key
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    protected $fillable = [
        'talks_id', 'attendees_id'
    ];
}
