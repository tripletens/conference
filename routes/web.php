<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', "FrontendController@landing");

Route::get('/talks/add', "FrontendController@add_talk_page")->name('add-talk-page');

Route::get('/talks/add/process', "FrontendController@process_add_talk")->name('process-add-talk');

Route::get('/talks', "FrontendController@view_talks");
