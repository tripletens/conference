<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'talks'], function () {
    Route::get('view-all', 'TalksController@view_talks');
    Route::post('view-joined-attendees', 'TalksController@view_joined_attendees');
    Route::post('add', 'TalksController@add_talk');
    Route::post('remove', 'TalksController@remove_talk');
});

Route::group(['prefix' => 'attendees'], function () {
    Route::post('add', 'AttendeeController@add_attendee');
    Route::post('join-talk', 'AttendeeController@join_talk');
});
